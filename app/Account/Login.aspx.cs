﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
    }

    protected void BbtnLogin_Click(object sender, EventArgs e)
    {
    //    dsWebDataContext ds = new dsWebDataContext();
    //    var q = from u in ds.aspnet_Users
    //            where u.UserName == txtUsername.Text && u.password == txtPass.Text
    //            select u;
    //    foreach (tblUser t in q)
    //    {
    //        _user = t.username;
    //        role = t.userlevel;
    //    }

    //    Session["user"] = _user;
    //    Session["role"] = role;

    //    if (txtUsername.Text == _user && role == "Administrator")
    //    {
    //        Response.Redirect("../Admin/Default.aspx");
    //    }
    //    else if(txtUsername.Text == _user && role == "Pakar")
    //    {
    //        Response.Redirect("../Pakar/Default.aspx");
    //    }
    }


    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        Session.Add("uid", cUser.getGuidByName(Login1.UserName));
        if(cGeneral.Db.aspnet_UsersInRoles_IsUserInRole(cGeneral.ApplicationName,Login1.UserName, cGeneral.RolePakarLiteral) > 0)
        {
            Session.Add("Role", cGeneral.RolePakarLiteral);
            Response.Redirect("~/Pakar/Default.aspx");
        }
        else if (cGeneral.Db.aspnet_UsersInRoles_IsUserInRole(cGeneral.ApplicationName, Login1.UserName, cGeneral.RoleAdministratorLiteral) > 0)
        {
            Session.Add("Role", cGeneral.RoleAdministratorLiteral);
            Response.Redirect("~/Admin/Default.aspx");
        }
    }
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
    }
}
