﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        if (Request.QueryString["UserId"] != null && Request.QueryString["secret"] != null)
        {
            bool success = cUser.confirmEmail(Guid.Parse(Request.QueryString["UserId"]), Request.QueryString["secret"]);
            if (success)
            {
                cGeneral.setMessage(this.Page, "Account has been activated successfully.");
            }
            else
            {
                cGeneral.setMessage(this.Page, "An error has occured.", cGeneral.MessageType.Error);
            }
            Response.Redirect("~/Account/Login.aspx");
        }
    }

    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {
        Guid UserId = cUser.getGuidByName(RegisterUser.UserName).Value;

        string ConfirmUrl = cGeneral.getAbsouluteUrl(Page, "/Account/Register.aspx?UserId=" + UserId + "&secret=" + cGeneral.calculateMd5Hash(RegisterUser.Email));
        cGeneral.sendMail(RegisterUser.Email, "Confirmation", "Pleaase click <a href='" + ConfirmUrl + "'>here</a> to activate your account.");
        cUser.AddAsPakar(UserId);
        //FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);
            
        //string continueUrl = RegisterUser.ContinueDestinationPageUrl;
        //if (String.IsNullOrEmpty(continueUrl))
        //{
        //    continueUrl = "~/";
        //}
        cGeneral.setMessage(this.Page, "User has been registered successfully. You must confirm your email to activate your account.", cGeneral.MessageType.Success);
        Response.Redirect("~/Account/Login.aspx");
    }

}
