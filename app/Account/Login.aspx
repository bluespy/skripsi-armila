﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <asp:Login ID="Login1" runat="server" CreateUserText="Register if you dont have an account." CreateUserUrl="~/Account/Register.aspx" InstructionText="Please enter your username and password." OnLoggedIn="Login1_LoggedIn">
            <TitleTextStyle Font-Bold="False" Font-Size="Large" />
        </asp:Login>
    </asp:Panel>
    </asp:Content>

