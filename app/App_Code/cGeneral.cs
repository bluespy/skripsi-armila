﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;

public class cGeneral
{
    public static dsWebDataContext Db = new dsWebDataContext();

    public static Guid ApplicationId
    {
        get
        {
            return Guid.Parse( ConfigurationManager.AppSettings["ApplicationId"].ToString() );
        }
    }

    public static string ApplicationName
    {
        get
        {
            return ConfigurationManager.AppSettings["ApplicationName"].ToString();
        }
    }

    public static Guid RolePakarId
    {
        get
        {
            return Guid.Parse( ConfigurationManager.AppSettings["Role_PakarId"] );
        }
    }

    public static string RolePakarLiteral
    {
        get
        {
            return ConfigurationManager.AppSettings["Role_PakarLiteral"];
        }
    }

    public static Guid RoleAdministratorId
    {
        get
        {
            return Guid.Parse(ConfigurationManager.AppSettings["Role_AdministratorId"]);
        }
    }

    public static string RoleAdministratorLiteral
    {
        get
        {
            return ConfigurationManager.AppSettings["Role_AdministratorLiteral"];
        }
    }
    
    public enum MessageType { Success, Warning, Error };

    public static string calculateMd5Hash(string Message)
    {
        MD5 crypto = MD5.Create();
        byte[] hash = crypto.ComputeHash(Encoding.ASCII.GetBytes(Message));

        StringBuilder sb = new StringBuilder();
        foreach (byte b in hash)
        {
            sb.Append(b.ToString("x2"));
        }

        return sb.ToString();
    }

    public static string implode(string[] textCollection, string delimiter, bool wrapWithQuote = true, string quoteType = "'" )
    {
        string outVar = "";
        foreach(string text in textCollection)
        {
            if (wrapWithQuote)
            {
                outVar += quoteType;
            }
            outVar += text;
            if (wrapWithQuote)
            {
                outVar += quoteType;
            }
            outVar += delimiter;
        }

        outVar = outVar.Substring(0, outVar.Length - delimiter.Length);

        return outVar;
    }

    public static void sendMail(string Recipient, string Subject, string Body)
    {
        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
        message.To.Add(Recipient);
        message.Subject = Subject;
        message.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["SMTPSender"].ToString());
        message.Body = Body;
        message.IsBodyHtml = true;
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPHost"].ToString());
        smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SMTPSender"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SMTPPass"].ToString());
        smtp.Send(message);
    }

    public static string getTypeLiteral(MessageType Type)
    {
        return Type == MessageType.Success ? "GFlashSuccess" : (Type == MessageType.Warning ? "GFlashWarning" : "GFlashError");
    }

    public static string getAbsouluteUrl(Page Parent, string Path = "")
    {
        string port = Parent.Request.Url.Port == 80 ? "" : Parent.Request.Url.Port.ToString();
        string appPath = Parent.Request.ApplicationPath == "/" ? "" : Parent.Request.ApplicationPath;
        if (Path[0] != '/')
        {
            Path = "/" + Path;
        }
        
        return Parent.Request.Url.Scheme + "://" + Parent.Request.Url.Host + ":" + port + "/" + Parent.Request.ApplicationPath + Path;
    }

    public static void setMessage(Page Parent, string Message, MessageType Type = MessageType.Success)
    {
        Parent.Session.Add(getTypeLiteral(Type), Message);
    }

    public static bool hasMessage(Page Parent, string Message, MessageType Type = MessageType.Success)
    {
        bool _out = false;

        if (Parent.Session[getTypeLiteral(Type)] != null)
        {
            if (Parent.Session[getTypeLiteral(Type)].ToString().Contains(Message))
            {
                return true;
            }
        }

        return _out;
    }

    public static string getMessage(Page Parent, MessageType Type = MessageType.Success, bool clearAfter = false)
    {
        string _out = "";

        if (Parent.Session[getTypeLiteral(Type)] != null)
        {
            if (Parent.Session[getTypeLiteral(Type)].ToString().Trim().Length > 0)
            {
                _out = Parent.Session[getTypeLiteral(Type)].ToString();
                if (clearAfter)
                {
                    Parent.Session.Remove(getTypeLiteral(Type));
                }
            }
        }

        return _out;
    }
        
    public static tblTransactCF[] getRC(string[] karakteristik, int limit = 5, bool useNames = false, bool includeRC = false)
    {
        int dataCount = karakteristik.Count();
        string dataCondition = cGeneral.implode(karakteristik, ",");
        string query = " SELECT TOP " + limit + " t1.kode_jenis KodeJenis, " +
                        " 		t1.TotalFound, " +
                        " 		t2.TotalAll, " +
                        " 		(1-(CAST(TotalAll AS FLOAT)-CAST(TotalFound AS FLOAT))/CAST(TotalAll AS FLOAT)) PercentageTotal, " +
                        " 		((CAST(TotalFound AS FLOAT))/" + dataCount + ") PercentageFound, " +
                        " 		( (1-(CAST(TotalAll AS FLOAT)-CAST(TotalFound AS FLOAT))/CAST(TotalAll AS FLOAT)) + ((CAST(TotalFound AS FLOAT))/" + dataCount + ") ) / 2 CF " +
                        " FROM " +
                        " ( " +
                        " SELECT t1.kode_jenis, COUNT(kode_jenis) TotalFound FROM tblKesimpulan t1 " +
                        " WHERE kode_karakteristik IN ("+ dataCondition +") " +
                        " GROUP BY kode_jenis " +
                        " ) t1 INNER JOIN ( " +
                        " SELECT kode_jenis, COUNT(kode_jenis) TotalAll FROM tblKesimpulan " +
                        " GROUP BY kode_jenis " +
                        " ) t2 ON t1.kode_jenis = t2.kode_jenis " +
                        " ORDER BY CF DESC ";
            
        SqlConnection sc = new SqlConnection(WebConfigurationManager.ConnectionStrings["ApplicationServices"].ToString());
        sc.Open();
        SqlCommand cmd = new SqlCommand(query, sc);
        SqlDataReader sdr = cmd.ExecuteReader();

        ds_db_mangrove db = new ds_db_mangrove();
        List<tblTransactCF> _out = new List<tblTransactCF>() ;

        while (sdr.Read())
        {
            tblTransactCF t = new tblTransactCF();
            t.KodeJenis = sdr["KodeJenis"].ToString();
            t.CertaintyFactor = double.Parse(sdr["CF"].ToString());
            if (useNames)
            {
                t.KodeJenis = cKarakteristik.getJenisName(t.KodeJenis);
                if (includeRC)
                {
                    t.KodeJenis += " (" + t.CertaintyFactor.ToString() + ")";
                }
            }

            _out.Add(t);
        }
        sc.Close();
        return _out.ToArray();
    }
}