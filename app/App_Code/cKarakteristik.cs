﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cKarakteristik
/// </summary>
public class cKarakteristik
{
    private static dsWebDataContext db = new dsWebDataContext();
    
    public cKarakteristik()
	{
		
	}

    public static int getKarakteristikGroupId(string KodeKarakteristik)
    {
        int _out = -1;

        var tbl = from tblKrkter t in db.tblKrkters
                  where t.kode_karakteristik == KodeKarakteristik
                  select t.GroupKarakteristikId;

        if (tbl != null)
        {
            _out = int.Parse(tbl.First().ToString());
        }

        return _out;
    }

    public static string getJenisName(string KodeJenis)
    {
        string _out = "";

        var tbl = from tblJenisM t in db.tblJenisMs
                  where t.kode_jenis == KodeJenis
                  select t.jenis_mangrove;

        if (tbl != null)
        {
            _out = tbl.First().ToString();
        }

        return _out;
    }

    public static string getNewKodeJenis()
    {
        string _out = "J";

        var tbl = from tblJenisM t in db.tblJenisMs
                  select t.kode_jenis;

        if (tbl != null)
        {
            _out += (int.Parse(tbl.AsEnumerable().ElementAt(tbl.Count() - 1).TrimStart('J')) + 1).ToString().PadLeft(2, '0');
        } else {
            _out += "0".PadLeft(2,'0');
        }

        return _out;
    }

    public static string getNewKodeKarakteristik()
    {
        string _out = "K";

        var tbl = from tblKrkter t in db.tblKrkters
                  select t.kode_karakteristik;

        if (tbl != null)
        {
            _out += (int.Parse(tbl.AsEnumerable().ElementAt(tbl.Count() - 1).TrimStart('K')) + 1).ToString().PadLeft(4, '0');
        }
        else
        {
            _out += "0".PadLeft(4, '0');
        }

        return _out;
    }

    public static void publishKodeJenis(string KodeJenis, bool Unpublish = false)
    {
        var tbl = from tblJenisM t in db.tblJenisMs
                  where t.kode_jenis == KodeJenis
                  select t;

        foreach (tblJenisM t in tbl)
        {
            t.published = 1;
            if (Unpublish)
            {
                t.published = 0;
            }
            db.SubmitChanges();
        }
    }

    public static void  publishArticle(int Id, bool Unpublish =false)
    {
        var tbl = from tblArtikel t in db.tblArtikels
                  where t.idArtikel == Id
                  select t;

        foreach (tblArtikel t in tbl)
        {
            t.asActive = 1;
            if (Unpublish)
            {
                t.asActive = 0;
            }

            db.SubmitChanges();
        }

    }
}