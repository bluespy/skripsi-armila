﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cUser
/// </summary>
public class cUser
{
    public static dsWebDataContext Db = new dsWebDataContext();
    public cUser()
	{
		
	}

    public static Guid? getGuidByName(string UserName)
    {
        var t = (from aspnet_User tbl in Db.aspnet_Users
                where tbl.UserName == UserName
                select tbl.UserId).First();
        if (t == null)
        {
            return null;
        }
        else
        {
            return Guid.Parse(t.ToString());
        }
    }

    public static bool AddAsPakar(Guid UserId)
    {
        try
        {
            Guid RoleId = Guid.Parse((from aspnet_Role tbl in Db.aspnet_Roles
                          where tbl.RoleName.Equals("Pakars")
                          select tbl.RoleId).First().ToString());

            aspnet_UsersInRole t = new aspnet_UsersInRole();
            t.RoleId = RoleId;
            t.UserId = UserId;

            Db.aspnet_UsersInRoles.InsertOnSubmit(t);
            Db.SubmitChanges();
        }
        catch
        {
        }

        return false;
    }

    public static bool confirmEmail(Guid UserId, string SecretHash)
    {
        try
        {
            var t = from aspnet_Membership tbl in Db.aspnet_Memberships
                    where tbl.UserId == UserId
                    select tbl;
            if (t != null)
            {
                foreach (aspnet_Membership tbl in t)
                {
                    if (cGeneral.calculateMd5Hash(tbl.Email) == SecretHash)
                    {
                        tbl.IsApproved = true;
                        Db.SubmitChanges();
                        return true;
                    }
                    
                }
            }
        }
        catch
        {
            
        }
        
        return false;
    }
}