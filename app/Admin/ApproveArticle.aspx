﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApproveArticle.aspx.cs" Inherits="approve" MasterPageFile="~/Shared/Admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Confirm for Artikel<br />
    <asp:GridView ID="dgvArtikel" runat="server" AutoGenerateColumns="False" DataKeyNames="idArtikel" DataSourceID="SqlArticles">
        <Columns>
            <asp:BoundField DataField="idArtikel" HeaderText="idArtikel" InsertVisible="False" ReadOnly="True" SortExpression="idArtikel" Visible="False" />
            <asp:BoundField DataField="judul" HeaderText="Judul" SortExpression="judul" />
            <asp:BoundField DataField="UserName" HeaderText="Pakar" SortExpression="UserName" />
            <asp:TemplateField HeaderText="Publish" SortExpression="AsActive">
                <ItemTemplate>
                    <asp:CheckBox ID="cbPublish" runat="server" Checked='<%# Eval("AsActive").ToString()=="0"?false:true %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlArticles" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommand="SELECT t2.idArtikel, t2.judul, t1.UserName, t2.AsActive FROM aspnet_Users AS t1 INNER JOIN tblArtikel AS t2 ON t1.UserId = t2.IdUser"></asp:SqlDataSource>
    <br />
    <asp:Button ID="btnbatal" runat="server" Text="Cancel" />
    <asp:Button ID="btnSave" runat="server" Text="Save" Width="58px" OnClick="btnSave_Click" />
</asp:Content>