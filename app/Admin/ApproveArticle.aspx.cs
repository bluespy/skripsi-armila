﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class approve : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dgvArtikel.Rows.Count; i++)
        {
            if (((CheckBox)dgvArtikel.Rows[i].Cells[dgvArtikel.Columns.Count - 1].FindControl("cbPublish")).Checked)
            {
                cKarakteristik.publishKodeJenis(dgvArtikel.Rows[i].Cells[0].ToString());
            }
            else
            {
                cKarakteristik.publishKodeJenis(dgvArtikel.Rows[i].Cells[0].ToString(), true);
            }
        }

        cGeneral.setMessage(Page, "Success", cGeneral.MessageType.Success);
    }
}