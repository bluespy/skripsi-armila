﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/Admin.Master" AutoEventWireup="true" CodeFile="ManagementUser.aspx.cs" Inherits="ManagementUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="Update_Main" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
    <table>
    <tr>
        <td>Username</td>
        <td>:</td>
        <td><asp:TextBox ID="txtUsername" runat="server" 
                ontextchanged="txtUsername_TextChanged"></asp:TextBox> </td>
                <td> 
                   <asp:RequiredFieldValidator ID="RFVUsername" runat="server" ControlToValidate="txtUsername" ValidationGroup="DefaultForm" ErrorMessage="Username is required." ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
    </tr>

    <tr>
        <td>Password</td>
        <td>:</td>
        <td><asp:TextBox ID="TxtPassword" TextMode="Password" runat="server"></asp:TextBox> </td>
        <td> 
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword" ValidationGroup="DefaultForm" ErrorMessage="Username is required." ForeColor="Red">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Email</td>
        <td>:</td>
        <td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox> </td>
    <td> 
       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" ValidationGroup="DefaultForm" ErrorMessage="Username is required." ForeColor="Red">*</asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td>User Level</td>
        <td>:</td>
        <td><asp:DropDownList ID="ddlUserlevel" runat="server" DataSourceID="LinqPakar" DataTextField="RoleName" DataValueField="RoleId">
            </asp:DropDownList> 
            <asp:LinqDataSource ID="LinqPakar" runat="server" ContextTypeName="dsWebDataContext" EntityTypeName="" Select="new (RoleId, RoleName)" TableName="aspnet_Roles">
            </asp:LinqDataSource>
        </td>
    </tr>
   
    <tr>
    <td colspan="3">
        <asp:Button ID="btnSimpan" runat="server" Text="Simpan" 
            onclick="btnSimpan_Click" ValidationGroup="DefaultForm" />
        <asp:Button ID="btnHapus" runat="server" Text="Hapus" 
            onclick="btnHapus_Click" />
        <asp:Button ID="btnBatal" runat="server" Text="Batal" 
            onclick="btnBatal_Click" />
        <asp:ConfirmButtonExtender ID="ConfirmButtonExtender_Delete" TargetControlID="btnHapus" ConfirmText="Apakah data akan dihapus?" runat="server">
        </asp:ConfirmButtonExtender>
        </td>
    </tr>
   
    </table>
    
    <asp:GridView ID="GridView_Main" runat="server" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" OnSelectedIndexChanged="GridView_Main_Selelcted"
        CellPadding="3" DataSourceID="SqlPakar">
        <Columns>
            <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="RoleName" HeaderText="RoleName" SortExpression="RoleName" />
            <asp:ButtonField CommandName="Select" Text="Pilih" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <PagerTemplate>
            <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
        </PagerTemplate>
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>
    
        <asp:SqlDataSource ID="SqlPakar" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Roles.RoleName FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId"></asp:SqlDataSource>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
