﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManagementUser : System.Web.UI.Page
{
    private dsWebDataContext db = new dsWebDataContext();
    public int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            refresh();
        }
    }

    public void refresh()
    {
        txtEmail.Text = String.Empty;
        txtUsername.Text = String.Empty;
        TxtPassword.Text = String.Empty;
    }

    #region GridView
    protected void GridView_Main_Selelcted(object sender, EventArgs e)
    {
        btnSimpan.Text = "Update";

        int id = Convert.ToInt32(GridView_Main.SelectedValue);
        _id = id;
        dsWebDataContext db = new dsWebDataContext();
        var q = from n in db.tblUsers
                where n.IdUser == id
                select n;
        foreach (tblUser t in q)
        {
            txtEmail.Text = t.email;
            txtUsername.Text = t.username;
            ddlUserlevel.SelectedItem.Text = t.userlevel;
            TxtPassword.Text = t.password;
        }
    }
    #endregion Gridview
    protected void btnSimpan_Click(object sender, EventArgs e)
    {
        //if (db.aspnet_Membership_GetUserByName(cGeneral.ApplicationName, txtUsername.Text, null, null) == null)
        //{
        //    db.aspnet_Membership_CreateUser(cGeneral.ApplicationName, txtUsername.Text

        //}
        if (btnSimpan.Text == "Simpan")
        {
            tblUser t = new tblUser();
            t.username = txtUsername.Text;
            t.password = TxtPassword.Text;
            t.email = txtEmail.Text;
            t.userlevel = ddlUserlevel.SelectedItem.Text;

            db.tblUsers.InsertOnSubmit(t);
            db.SubmitChanges();
            // UpdatePanelGrid.Update();
            refresh();
        }
        else
        {
            var q = from n in db.tblUsers
                    where n.IdUser == _id
                    select n;
            foreach (tblUser t in q)
            {
                t.email = txtEmail.Text;
                t.username = txtUsername.Text;
                t.userlevel = ddlUserlevel.SelectedItem.Text;
                t.password = TxtPassword.Text;
            }
            try
            {
                db.SubmitChanges();
            }
            catch
            { 
            }
            //UpdatePanelGrid.Update();
            refresh();
            btnSimpan.Text = "Simpan";
        }
    }

    protected void txtUsername_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnBatal_Click(object sender, EventArgs e)
    {
        refresh();
    }

    protected void btnHapus_Click(object sender, EventArgs e)
    {
        dsWebDataContext db = new dsWebDataContext();
        var del = from n in db.tblUsers
                    where n.IdUser == _id
                    select n;
        foreach (var delete in del)
        {
            db.tblUsers.DeleteOnSubmit(delete);
        }
        try
        {
            db.SubmitChanges();
        }
        catch
        { 
        }
    }
}