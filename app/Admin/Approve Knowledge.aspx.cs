﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class approve2 : System.Web.UI.Page
{
    private dsWebDataContext db = new dsWebDataContext();
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dgvConfirm.Rows.Count; i++)
        {
            if (((CheckBox)dgvConfirm.Rows[i].Cells[dgvConfirm.Columns.Count - 1].FindControl("cbPublished")).Checked)
            {
                cKarakteristik.publishKodeJenis(dgvConfirm.Rows[i].Cells[0].ToString());
            }
            else
            {
                cKarakteristik.publishKodeJenis(dgvConfirm.Rows[i].Cells[0].ToString(), true);
            }
        }

        cGeneral.setMessage(Page, "Success", cGeneral.MessageType.Success);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        List<string> l = new List<string>();
        
        for (int i = 0; i < dgvConfirm.Rows.Count; i++)
        {
            if (((CheckBox)dgvConfirm.Rows[i].Cells[dgvConfirm.Columns.Count - 1].FindControl("cbPublished")).Checked)
            {
                cKarakteristik.publishKodeJenis(dgvConfirm.Rows[i].Cells[0].ToString());
                l.Add(dgvConfirm.Rows[i].Cells[0].ToString());
            }
            else
            {
                cKarakteristik.publishKodeJenis(dgvConfirm.Rows[i].Cells[0].ToString(), true);
            }
        }

        var t = from tblJenisM tbl in db.tblJenisMs
                where l.Contains(tbl.kode_jenis)
                select tbl;

        foreach (tblJenisM tbl in t)
        {
            db.tblJenisMs.DeleteOnSubmit(tbl);
            db.SubmitChanges();
        }
    }
}