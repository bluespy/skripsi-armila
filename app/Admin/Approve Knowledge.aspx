﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Approve Knowledge.aspx.cs" Inherits="approve2" MasterPageFile="~/Shared/Admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>    
        Confirm for new base knowledge<br />
        <asp:GridView ID="dgvConfirm" runat="server" AutoGenerateColumns="False" DataKeyNames="kode_jenis" DataSourceID="sqlKnowledgeBase">
            <Columns>
                <asp:BoundField DataField="kode_jenis" HeaderText="kode_jenis" ReadOnly="True" SortExpression="kode_jenis" Visible="False" />
                <asp:BoundField DataField="UserName" HeaderText="Nama Pakar" SortExpression="UserName" />
                <asp:BoundField DataField="jenis_mangrove" HeaderText="Jenis Mangrove" SortExpression="jenis_mangrove" />
                <asp:BoundField DataField="Karakteristik" HeaderText="Karakteristik" ReadOnly="True" SortExpression="Karakteristik" />
                <asp:TemplateField HeaderText="Confirm" SortExpression="published">
                    <ItemTemplate>
                        <!-- <asp:Label ID="lbl" runat="server" Text='<%# Bind("published") %>'></asp:Label> -->
                        <asp:CheckBox ID="cbPublished" Checked='<%# Eval("published").ToString() == "0" ? false : true  %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="sqlKnowledgeBase" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommand="SELECT t1.*,t3.UserName,
SUBSTRING((SELECT ', ' + t4.karakteristik FROM tblKesimpulan t2 INNER JOIN tblKrkter t4
	ON t2.kode_karakteristik=t4.kode_karakteristik
 WHERE t2.kode_jenis=t1.kode_jenis For XML PATH('')),3,10000) Karakteristik
FROM tblJenisM t1 LEFT OUTER JOIN aspnet_Users t3 ON t1.id_pakar = t3.UserId"></asp:SqlDataSource>
        <br />
        <asp:Button ID="btnbatal" runat="server" Text="Cancel" />
        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="Delete" />
        <asp:Button ID="btnSave" runat="server" Text="Save" Width="58px" OnClick="btnSave_Click" />
    </div>
</asp:Content>