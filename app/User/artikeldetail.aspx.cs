﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_artikelajah : System.Web.UI.Page
{
    private dsWebDataContext db = new dsWebDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["id"].ToString().Length > 0)
            {
                tblArtikel t = (from tblArtikel tbl in db.tblArtikels
                        where tbl.idArtikel == int.Parse(Request.QueryString["id"].ToString())
                        select tbl).First();

                if (t != null)
                {
                    txtJudul.Text = t.judul;
                    txtIsi.Text = t.isi;
                    lblIdArtikel.Value = t.idArtikel.ToString();
                }
            }
            Response.Redirect("~/User/Artikel.aspx");
        }
        catch
        {
            Response.Redirect("~/User/Artikel.aspx");
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        tblComment t = new tblComment();
        t.Email = txtEmail.Text;
        t.IdThread = int.Parse(lblIdArtikel.Value);
        t.Isi = txtIsi.Text;
        t.Name = txtName.Text;
        t.postingdate = DateTime.Now;

        db.tblComments.InsertOnSubmit(t);
        db.SubmitChanges();
    }
}