﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebIdentifikasitanamanmangrove.User
{
    public partial class Identifikasi : System.Web.UI.Page
    {
        dsWebDataContext db = new dsWebDataContext();

        private void showControl(bool? asLast = null)
        {
            btnNext.Visible = false;
            btnPrev.Visible = false;
            btnFinish.Visible = false;
            
            if (asLast == null)
            {
                btnNext.Visible = true;
                btnPrev.Visible = true;
            }
            else if (asLast != true)
            {
                btnNext.Visible = true;
            }
            else
            {
                btnPrev.Visible = true;
                btnFinish.Visible = true;
            }
        }

        private void RefreshCheckLists(int? Id = null, bool asNext = false)
        {
            var coll = db.GetTable(typeof(tblGroupKarakteristik));
            List<tblGroupKarakteristik> gkColl = new List<tblGroupKarakteristik>();

            foreach (tblGroupKarakteristik row in coll)
            {
                gkColl.Add(row);
            }

            if (Id == null)
            {
                hfCurrentId.Value = gkColl[0].Id.ToString();
                lblGroupKarakteristik.Text = gkColl[0].Name;
                showControl(false);
            }
            else
            {
                for (int i = 0; i < gkColl.Count; i++)
                {                   
                    if (gkColl[i].Id == Id)
                    {
                        if (asNext)
                        {
                            hfCurrentId.Value = gkColl[i + 1].Id.ToString();
                            lblGroupKarakteristik.Text = gkColl[i + 1].Name;
                        }
                        else
                        {
                            hfCurrentId.Value = gkColl[i - 1].Id.ToString();
                            lblGroupKarakteristik.Text = gkColl[i - 1].Name;
                        }
                        
                        if (hfCurrentId.Value == gkColl[gkColl.Count - 1].Id.ToString()) //last
                        {
                            showControl(true);
                        }
                        else if (hfCurrentId.Value == gkColl[0].Id.ToString()) // first
                        {
                            showControl(false);
                        }
                        else
                        {
                            showControl();
                        }
                    }
                }   
            }
        }

        private void gatherData()
        {
            for (int i = 0; i < cbKarakteristik.Items.Count; i++)
            {
                if (cbKarakteristik.Items[i].Selected)
                {
                    if (!hfResults.Value.Contains(cbKarakteristik.Items[i].Value.Trim()))
                    {
                        hfResults.Value += cbKarakteristik.Items[i].Value.Trim() + ",";
                    }
                }
            }
        }

        private void showPanels(bool asResult = false)
        {
            pnlResults.Visible = false;
            pnlQuestions.Visible = false;

            if (asResult)
            {
                pnlResults.Visible = true;
            }
            else
            {
                pnlQuestions.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {   
            if (Request.Params.ToString().Contains("btnSendEmail"))
            {
                string body = "Hasil pengujian: <br />";

                int i = 1;
                foreach (tblTransactCF tbl in cGeneral.getRC(hfResults.Value.Split(','), 2))
                {
                    body += i++.ToString() + ". " + cKarakteristik.getJenisName(tbl.KodeJenis) + " (" + tbl.CertaintyFactor.ToString() + ") <br />";
                }

                cGeneral.sendMail(txtEmail.Text, "Result", body);
                
                cGeneral.setMessage(this.Page, "Email sent.");
                Response.Redirect("~/Default.aspx");
            } else if (Request.Params.ToString().Contains("btnFinish"))
            {
                string[] results = hfResults.Value.Split(',');
                Session.Add("Results", results);

                var coll = db.GetTable(typeof(tblGroupKarakteristik));
                
                foreach (tblGroupKarakteristik tbl in coll)
                {
                    Label lblGroupKarakteristik = new Label();
                    lblGroupKarakteristik.Text = "<br />" + tbl.Name + "<br />";
                    lblGroupKarakteristik.Font.Bold = true;
                    pnlResultsDetail.Controls.Add(lblGroupKarakteristik);

                    foreach (string result in results)
                    {
                        if (result.Trim().Length > 0)
                        {
                            if (cKarakteristik.getKarakteristikGroupId(result) == tbl.Id)
                            {
                                var t = from tblKrkter tbl2 in cGeneral.Db.tblKrkters
                                        where tbl2.GroupKarakteristikId == tbl.Id
                                        select tbl2.karakteristik;

                                if (t != null)
                                {
                                    Label lblKarakteristik = new Label();
                                    lblKarakteristik.Text = t.First().ToString() + "<br />";
                                    pnlResultsDetail.Controls.Add(lblKarakteristik);
                                }

                            }
                        }
                    }
                }

                showPanels(true);
            }
            else
            {
                RefreshCheckLists(hfCurrentId.Value.Length == 0 ? (int?)null : int.Parse(hfCurrentId.Value), Request.Params.ToString().Contains("btnNext"));
                showPanels();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
            
            //NameValueCollection request = Request.Form;

            //for (int i = 0; i < request.Count; i++)
            //{
            //    if (request.Keys[i] == "karakteristik[]")
            //    {
            //        //Label1.Text += request[i];
            //    }
            //}

            //lblQuestionsContainer.Text = "";
            //foreach(tblTransactCF tbl in cGeneral.getRC(new string[] { "K0101", "K0402","K0103","K0102","K0202","K0102","K1202" } ))
            //{
            //    lblQuestionsContainer.Text = "";
            //}

            
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            gatherData();
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            gatherData();
        }

        protected void getKuesionerQuestions(int? Id = null)
        {
            List<int> groupKarakteristik = new List<int>();
            foreach(tblGroupKarakteristik row in db.GetTable(typeof(tblGroupKarakteristik)))
            {
                groupKarakteristik.Add(row.Id);
            }
            
            ViewState.Add("groupKarakteristik", groupKarakteristik);
        }

        protected void cbKarakteristik_DataBound(object sender, EventArgs e)
        {
            string[] Results = hfResults.Value.Split(',');
            for (int j = 0; j < cbKarakteristik.Items.Count; j++)
            {
                for (int i = 0; i < Results.Count(); i++)
                {
                    if (cbKarakteristik.Items[j].Value.Trim() == Results[i])
                    {
                        cbKarakteristik.Items[j].Selected = true;
                    }
                }
            }
        }
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            
        }
}
}