﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Identifikasi.aspx.cs" Inherits="WebIdentifikasitanamanmangrove.User.Identifikasi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    
    <asp:Panel ID="pnlQuestions" runat="server">
        <asp:Label ID="lblGroupKarakteristik" runat="server" Text="Label"></asp:Label>
        <asp:CheckBoxList ID="cbKarakteristik" runat="server" DataSourceID="dsKarakteristiks" DataTextField="karakteristik" DataValueField="kode_karakteristik" OnDataBound="cbKarakteristik_DataBound">
        </asp:CheckBoxList>
        <asp:LinqDataSource ID="dsKarakteristiks" runat="server" ContextTypeName="dsWebDataContext" EntityTypeName="" TableName="tblKrkters" Where="GroupKarakteristikId == @GroupKarakteristikId">
            <WhereParameters>
                <asp:ControlParameter ControlID="hfCurrentId" Name="GroupKarakteristikId" PropertyName="Value" Type="Int32" />
            </WhereParameters>
        </asp:LinqDataSource>
        <asp:HiddenField ID="hfCurrentId" runat="server" />
        <asp:HiddenField ID="hfResults" runat="server" />
        <asp:Button ID="btnPrev" runat="server" Text="Previous" OnClick="btnPrev_Click" />
        <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
        <asp:Button ID="btnFinish" runat="server" Text="Finish" OnClick="btnSubmit_Click" />
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlResults" runat="server">
        <asp:Label ID="lblResult" runat="server" Text="Karakteristik yang diinputkan:"></asp:Label>
        <asp:Panel ID="pnlResultsDetail" runat="server">
        </asp:Panel>
        <asp:Label ID="Label1" runat="server" Text="Hasil identifikasi menurut karakter yang diinputkan:"></asp:Label>
        <br />
        <asp:BulletedList ID="BulletedList1" runat="server" BulletStyle="Numbered" DataSourceID="ObjectDataSource1" DataTextField="KodeJenis" DataTextFormatString="{0}" />
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="getRC" TypeName="cGeneral">
            <SelectParameters>
                <asp:SessionParameter Name="karakteristik" SessionField="Results" Type="Object" />
                <asp:Parameter DefaultValue="2" Name="limit" Type="Int32" />
                <asp:Parameter DefaultValue="true" Name="useNames" Type="Boolean" />
                <asp:Parameter DefaultValue="true" Name="includeRC" Type="Boolean" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Result will be sent to your account"></asp:Label>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Email address: "></asp:Label>
        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        <asp:Button ID="btnSendEmail" runat="server" OnClick="btnSendEmail_Click" Text="Send" />
    </asp:Panel>
    <br />
</asp:Content>
