﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Artikel.aspx.cs" Inherits="WebIdentifikasitanamanmangrove.User.Artikel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <asp:DataList ID="dlArticle" runat="server" DataSourceID="LinqArticle" OnSelectedIndexChanged="dlArticle_SelectedIndexChanged">
            <ItemTemplate>
                <asp:HiddenField ID="idArtikelLabel" runat="server" Value='<%# Eval("idArtikel") %>' />
                <h2>
                    <asp:Label ID="judulLabel" runat="server" Font-Bold="True" Font-Size="Medium" Text='<%# Eval("judul") %>' />
                </h2>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("isi") %>' />
                <br />
                <p style="text-align: right">
                    <asp:HyperLink runat="server" NavigateUrl='~/User/artikeldetail.aspx?id=<%# Eval("idArtikel") %>' Text="Read More..." /> &nbsp;
                    <asp:HyperLink runat="server" NavigateUrl='~/User/artikeldetail.aspx?id=<%# Eval("idArtikel") %>' Text="Tulisan Komentar" />
                </p>
                <hr />
            </ItemTemplate>
        </asp:DataList>
        <asp:LinqDataSource ID="LinqArticle" runat="server" ContextTypeName="dsWebDataContext" EntityTypeName="" Select="new (idArtikel, judul, isi)" TableName="tblArtikels">
        </asp:LinqDataSource>
        <br />
    </p>
</asp:Content>
