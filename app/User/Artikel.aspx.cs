﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebIdentifikasitanamanmangrove.User
{
    public partial class Artikel : System.Web.UI.Page
    {
        private dsWebDataContext db = new dsWebDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            tblComment t = new tblComment();
            t.Email = ((TextBox)dlArticle.Items[0].FindControl("txtEmail")).Text;
            t.IdThread = int.Parse(((HiddenField)dlArticle.Items[0].FindControl("idArtikelLabel")).Value);
            t.Isi = ((TextBox)dlArticle.Items[0].FindControl("txtComment")).Text; ;
            t.Name = ((TextBox)dlArticle.Items[0].FindControl("txtName")).Text;
            t.postingdate = DateTime.Now;

            db.tblComments.InsertOnSubmit(t);
            db.SubmitChanges();
        }
        protected void dlArticle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
}
}