﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string _role = (string)Session["role"];
        if (_role == null || _role != cGeneral.RoleAdministratorLiteral)
        {
            cGeneral.setMessage(this.Page, "You are not authorized.", cGeneral.MessageType.Error);
            Response.Redirect("~/Default.aspx");
        }

        List<cGeneral.MessageType> _tmpCol = new List<cGeneral.MessageType>() { cGeneral.MessageType.Error, cGeneral.MessageType.Warning, cGeneral.MessageType.Success };
        List<Label> _tmpLblCol = new List<Label>() { FlashError, FlashWarning, FlashSuccess };

        for (int i = 0; i < _tmpCol.Count; i++)
        {
            _tmpLblCol[i].Visible = false;
            if (cGeneral.getMessage(this.Page, _tmpCol[i]).ToString().Length > 0)
            {
                _tmpLblCol[i].Text = cGeneral.getMessage(this.Page, _tmpCol[i], true).ToString();
                _tmpLblCol[i].Visible = true;
            }
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("../Logout.aspx");
    }
}