﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pakar_addbaseknowledge : System.Web.UI.Page
{
    private dsWebDataContext db = new dsWebDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.Params.ToString().Contains(btnsave.Text))
        {
            if(!cGeneral.hasMessage(Page, "harus diisi", cGeneral.MessageType.Error))
            {
                cGeneral.setMessage(Page, "Success");
            }
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (txtJenisMangrove.Text.Length == 0)
        {
            cGeneral.setMessage(Page, "Jenis Mangrove harus diisi.", cGeneral.MessageType.Error);
            //Response.Redirect("~/Pakar/KnowledgeBase.aspx");
            return;
        }
        tblJenisM tJ = new tblJenisM();
        tJ.kode_jenis = cKarakteristik.getNewKodeJenis();
        tJ.jenis_mangrove = txtJenisMangrove.Text;
        tJ.id_pakar = Guid.Parse(Session["uid"].ToString());
        db.tblJenisMs.InsertOnSubmit(tJ);
        db.SubmitChanges();

        for (int i = 0; i < dgvKarakteristik.Rows.Count - 1; i++)
        {
            if (((CheckBox)dgvKarakteristik.Rows[i].Cells[dgvKarakteristik.Columns.Count - 1].FindControl("cbSelected")).Checked)
            {
                tblKrkter tKrktr = new tblKrkter();
                string txtKarakteristik = ((TextBox)dgvKarakteristik.Rows[i].Cells[dgvKarakteristik.Columns.Count-1].FindControl("txtKarakteristik")).Text;
                if (txtKarakteristik.Trim().Length > 0)
                {
                    tKrktr.karakteristik = txtKarakteristik;
                    tKrktr.GroupKarakteristikId = int.Parse(dgvKarakteristik.Rows[i].Cells[0].Text);
                    tKrktr.kode_karakteristik = cKarakteristik.getNewKodeKarakteristik();

                    db.tblKrkters.InsertOnSubmit(tKrktr);
                    db.SubmitChanges();
                }
            
                tblKesimpulan tK = new tblKesimpulan();
                tK.kode_karakteristik = txtKarakteristik.Trim().Length > 0 ? tKrktr.kode_karakteristik : ((DropDownList)dgvKarakteristik.Rows[i].Cells[dgvKarakteristik.Columns.Count-1].FindControl("ddlKarakteristik")).SelectedValue.ToString().Trim();

                tK.kode_jenis = tJ.kode_jenis;
                db.tblKesimpulans.InsertOnSubmit(tK);
                db.SubmitChanges();
            }
        }

        Response.Redirect("~/Pakar/KnowledgeBase.aspx");
    }

    protected void cbSelected_CheckedChanged(object sender, EventArgs e)
    {
        
    }
}