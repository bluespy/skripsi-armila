﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebIdentifikasitanamanmangrove.Pakar
{
    public partial class Artikel : System.Web.UI.Page
    {
        private dsWebDataContext db = new dsWebDataContext();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            tblKategori t = new tblKategori();
            if (!cmbKategoriId.Visible)
            {
                t.kategori = txtKategori.Text;
                db.tblKategoris.InsertOnSubmit(t);
                db.SubmitChanges();
            }

            tblArtikel t2 = new tblArtikel();
            t2.asActive = 0;
            t2.Iduser = Guid.Parse(Session["uid"].ToString());
            t2.isi = txtIsi.Text;
            t2.judul = txtJudul.Text;
            t2.kategoriId = !cmbKategoriId.Visible ? t.kategoriId : int.Parse(cmbKategoriId.SelectedValue.ToString());
            t2.tanggalpost = DateTime.Now;
            db.tblArtikels.InsertOnSubmit(t2);
            db.SubmitChanges();

            //ds_db_mangroveTableAdapters.tblArtikelTableAdapter tAdapter = new ds_db_mangroveTableAdapters.tblArtikelTableAdapter();
            //tAdapter.Insert(txtJudul.Text, txtIsi.Text, (int?)int.Parse(cmbKategoriId.SelectedValue), (DateTime?)DateTime.Now, (Guid?) Guid.Parse(Session["uid"].ToString()), byte.Parse(!cmbKategoriId.Visible? t.kategoriId.ToString() : cmbKategoriId.SelectedValue.ToString()));
            Response.Redirect("~/Pakar/Artikel.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        private void showCancel()
        {
            lbNewCategory.Text = "Cancel";
            cmbKategoriId.Visible = false;
            txtKategori.Visible = true;
        }

        private void showAdd()
        {
            lbNewCategory.Text = "Add New";
            cmbKategoriId.Visible = true;
            txtKategori.Visible = false;
        }

        protected void lbNewCategory_Click(object sender, EventArgs e)
        {
            if (lbNewCategory.Text.ToLower().Contains("new"))
            {
                showCancel();
            }
            else
            {
                showAdd();
            }
        }
}
}