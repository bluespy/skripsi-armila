﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/Pakar.Master" AutoEventWireup="true" CodeFile="karakter.aspx.cs" Inherits="Karakter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 386px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="style1">
                kode karakteristik</td>
            <td>
                :</td>
            <td>
                <asp:TextBox ID="txtkodekarakteristik" runat="server" Height="17px" 
                    Width="130px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                karakteristik</td>
            <td>
                :</td>
            <td>
                <asp:TextBox ID="txtKarakteristik" runat="server" Height="17px" Width="130px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                nilai cf</td>
            <td>
                :</td>
            <td>
                <asp:TextBox ID="txtNilaiCf" runat="server" Height="17px" Width="130px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="margin-left: 40px">
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
             
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="kode_karakteristik" DataSourceID="sqlkaraketristik">
        <Columns>
            <asp:BoundField DataField="kode_karakteristik" HeaderText="kode_karakteristik" 
                ReadOnly="True" SortExpression="kode_karakteristik" />
            <asp:BoundField DataField="karakteristik" HeaderText="karakteristik" 
                SortExpression="karakteristik" />
            <asp:BoundField DataField="nilai_cf" HeaderText="nilai_cf" 
                SortExpression="nilai_cf" />
            <asp:BoundField DataField="GroupKarakteristikId" 
                HeaderText="GroupKarakteristikId" SortExpression="GroupKarakteristikId" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlkaraketristik" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
        SelectCommand="SELECT * FROM [tblKrkter]"></asp:SqlDataSource>
    <br />
</asp:Content>
