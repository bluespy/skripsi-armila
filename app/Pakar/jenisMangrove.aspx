﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/Pakar.Master" AutoEventWireup="true" CodeFile="jenisMangrove.aspx.cs" Inherits="WebIdentifikasitanamanmangrove.Pakar.JenisMangrove" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 172px;
        }
        .style2
        {
            width: 81px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="style1">
                kode_jenis</td>
            <td class="style2">
                :</td>
            <td>
                <asp:TextBox ID="KodeJenis" runat="server" Height="17px" Width="130px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                jenis mangrove</td>
            <td class="style2">
                :</td>
            <td>
                <asp:TextBox ID="txtJnsMagrve" runat="server" Height="17px" Width="130px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="margin-left: 40px">
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
             
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="margin-left: 40px">
                &nbsp;</td>
        </tr>
    </table>
<br />
<asp:GridView ID="GridView1" runat="server" DataSourceID="dsJenisMangrove" 
        AutoGenerateColumns="False" DataKeyNames="kode_jenis" 
        style="margin-right: 77px">
    <Columns>
        <asp:BoundField DataField="kode_jenis" HeaderText="kode_jenis" ReadOnly="True" 
            SortExpression="kode_jenis" />
        <asp:BoundField DataField="jenis_mangrove" HeaderText="jenis_mangrove" 
            SortExpression="jenis_mangrove" />
    </Columns>
</asp:GridView>
    <asp:SqlDataSource ID="dsJenisMangrove" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
        SelectCommand="SELECT * FROM [tblJenisM]"></asp:SqlDataSource>
</asp:Content>
