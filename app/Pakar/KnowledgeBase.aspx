﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KnowledgeBase.aspx.cs" Inherits="Pakar_addbaseknowledge" MasterPageFile="~/Shared/Pakar.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1, .auto-style3
        {
            width: 25px;
        }
        .auto-style3
        {
            height: 22px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <br />
        Jenis Mangrove:
        <asp:TextBox ID="txtJenisMangrove" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:GridView ID="dgvKarakteristik" runat="server" AutoGenerateColumns="False" DataSourceID="linqCharacteristicGroup">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Name" HeaderText="Data Karakteristik" ReadOnly="True" SortExpression="Name" />
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlKarakteristik" runat="server" DataSourceID="linqKarakteristik" DataTextField="karakteristik" DataValueField="kode_karakteristik">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtKarakteristik" runat="server"></asp:TextBox>
                        <asp:CheckBox ID="cbSelected" runat="server" Checked="True" OnCheckedChanged="cbSelected_CheckedChanged" Text=" " />
                        <asp:LinqDataSource ID="linqKarakteristik" runat="server" ContextTypeName="dsWebDataContext" EntityTypeName="" Select="new (karakteristik, kode_karakteristik)" TableName="tblKrkters" Where='<%# "GroupKarakteristikId == " + Eval("Id") %>' >
                        </asp:LinqDataSource>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:LinqDataSource ID="linqCharacteristicGroup" runat="server" ContextTypeName="dsWebDataContext" EntityTypeName="" Select="new (Id, Name)" TableName="tblGroupKarakteristiks">
        </asp:LinqDataSource>
        <br />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" />
        <asp:Button ID="btnsave" runat="server" Text="Add" Width="62px" OnClick="btnsave_Click" />
    
    </div>
</asp:Content>