﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/Pakar.Master" AutoEventWireup="true" CodeFile="Artikel.aspx.cs" Inherits="WebIdentifikasitanamanmangrove.Pakar.Artikel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="width:100%;">
            <tr>
                <td>
                    judul</td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtJudul" runat="server" Height="17px" Width="146px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    isi</td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtIsi" runat="server" Height="117px" TextMode="MultiLine" Width="345px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    kategori Id</td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="cmbKategoriId" runat="server" DataSourceID="ObjectDataSource1" DataTextField="kategori" DataValueField="kategoriId">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtKategori" runat="server" Visible="False"></asp:TextBox>
                    <asp:LinkButton ID="lbNewCategory" runat="server" OnClick="lbNewCategory_Click">Add New</asp:LinkButton>
&nbsp;<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ds_db_mangroveTableAdapters.tblKategoriTableAdapter" UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_kategoriId" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="kategoriId" Type="Int32" />
                            <asp:Parameter Name="kategori" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="kategori" Type="String" />
                            <asp:Parameter Name="Original_kategoriId" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="margin-left: 40px">
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
             
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                <asp:Button ID="btnDelete" runat="server" onclick="Button1_Click" 
                    Text="Delete" />
            </td>
        </tr>
        </table>
    </div>
        <p>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="idArtikel" DataSourceID="sqlArtikel" 
                EmptyDataText="There are no data records to display.">
                <Columns>
                    <asp:BoundField DataField="idArtikel" HeaderText="idArtikel" ReadOnly="True" 
                        SortExpression="idArtikel" />
                    <asp:BoundField DataField="judul" HeaderText="judul" SortExpression="judul" />
                    <asp:BoundField DataField="isi" HeaderText="isi" SortExpression="isi" />
                    <asp:BoundField DataField="Iduser" HeaderText="Iduser" 
                        SortExpression="Iduser" />
                    <asp:BoundField DataField="kategoriId" HeaderText="kategoriId" 
                        SortExpression="kategoriId" />
                    <asp:BoundField DataField="tanggalpost" HeaderText="tanggalpost" 
                        SortExpression="tanggalpost" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="sqlArtikel" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                SelectCommand="SELECT * FROM [tblArtikel]"></asp:SqlDataSource>
    </p>
</asp:Content>
